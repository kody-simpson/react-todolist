import Item from './components/Item.js';
import React from 'react';
import './styles/main.css';
import './App.css';

class App extends React.Component{

  state = {
    input: '',
    items: [
      'Walk my seagul',
      'Legalize murder',
      'Listen to Dean Martin'
    ]
  }

  constructor(props){
    super(props);

    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }


  handleInput(event){

    this.setState(({
      input: event.target.value
    }));
  
  }

  addItem(event){
    
    this.setState((state) => (
      {
        items: state.items.concat(state.input)
      }
    ));

    event.preventDefault();

  }

  removeItem(){

    let newList = this.state.items;
    newList.pop();

    this.setState((state) => (
      {
        items: newList
      }
    ))

  }

  render() {
    return (
      <div className="App">
  
        <h2 className="bg-purple-600 text-white text-center max-w-2xl font-mono shadow-xl text-5xl font-medium m-auto mt-5 mb-5 p-5 border-gray-400 rounded-lg">My Todo List:</h2>
  
        <List items={this.state.items} action={this.removeItem}></List>
        
        <div className="flex justify-center">
          <form onSubmit={this.addItem}>
            <div className="space-x-5">
                <input placeholder="Walk my dog" className="pl-2 p-2 border-2 border-purple-500 rounded-lg w-64" type="text" value={this.state.input} onChange={this.handleInput}></input>
                <button className="hover:bg-gray-50 hover:text-purple-800 bg-pink-500 rounded-md text-white px-5 text-xl py-1" type="submit" name="action">Add</button>
            </div>
            
          </form>
        </div>
  
      </div>
    );
  }

}

function List(props){
  return (
    <div className="grid justify-items-center">
      {props.items.map((item, index) => {
        return (
          <Item name={item} key={index} action={props.action}></Item>
        );
      })}
    </div>
  )
}

export default App;
